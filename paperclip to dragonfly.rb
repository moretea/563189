require "config/environment"

count = Rim.count
Rim.all.each_with_index do |rim, i|
  if rim.rim_photo.file?
    puts "rim #{i} of #{count} +"
    rim.photo = File.open((Rails.root.to_s + '/public' + rim.rim_photo.url).split("?").first)
    rim.save
  else
    puts "rim #{i} of #{count} -"
end
  